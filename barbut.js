console.log("Do I still know how to use this thing?");

const rollBtn = document.querySelector(".roll");

function dice(sides) {
	return 1 + Math.floor(Math.random() * sides);
}

rollBtn.addEventListener("click", function() {
	console.log(dice(6));
});